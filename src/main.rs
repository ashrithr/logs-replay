extern crate clap;
#[macro_use]
extern crate slog;
extern crate slog_async;
extern crate slog_term;

use clap::{App, Arg};
use slog::*;

use std::fs::File;
use std::io::{BufRead, BufReader};
use std::io::prelude::*;
use std::net::{TcpStream, UdpSocket, Shutdown};
use std::time::{Duration, Instant};
use std::thread;
use std::str;
use std::net::TcpListener;
use std::sync::{Mutex, Arc};

const BUFFER_SIZE: usize = 8096;
const DEFAULT_DELIMITER: &'static str = "\n";

/// struct to hold file statistics
#[derive(Debug)]
struct FileStats {
    file_size: usize,
    num_lines: usize,
    max_file_length: usize
}

/// struct to hold printer thread statistics
#[derive(Debug)]
struct PrintStats {
    events: u64,
    bytes: u64,
    log: Logger,
}

impl PrintStats {
    pub fn new(log: Logger) -> PrintStats {
        PrintStats { events: 0, bytes: 0, log: log }
    }

    pub fn print(&self) {
        debug!(&self.log, "Print Stats Counter : {} (events/buffers) [{} bytes]", self.events, self.bytes)
    }

    pub fn increment_event(&mut self) {
        self.events += 1
    }

    pub fn update_bytes(&mut self, update_value: u64) {
        self.bytes += update_value
    }
}

fn main() {

    if cfg!(debug_assertions) {
        println!("!! WARNING: You are running a not optimized version of the logs-replay !!");
        println!("!! Pleae use the --release build switch for optimized performance !!");
        println!("");
    }

    // initialize the logger
    let decorator = slog_term::PlainDecorator::new(std::io::stdout());
    let drain = slog_term::FullFormat::new(decorator).build().fuse();
    let drain = slog_async::Async::new(drain).build().fuse();
    let log = Logger::root(drain.fuse(), o!());

    // define and initialize the command line argument parser
    let matches = App::new("logs-replay")
        .about("Logs replay is a simple tool to replay log events from a specified file over to \
            a specified TCP or UDP port")
        .arg(Arg::with_name("eps")
            .short("e")
            .long("eps")
            .help("Specifies how fast you want to replay the events per second")
            .default_value("1000")
        )
        .arg(Arg::with_name("total")
            .short("t")
            .long("total-events")
            .help("Specifies total number of events to replay. Default: number of lines in \
                the file")
            .takes_value(true)
        )
        .arg(Arg::with_name("host")
            .short("h")
            .long("hostname")
            .help("hostname to replay to")
            .default_value("127.0.0.1")
        )
        .arg(Arg::with_name("port")
            .short("p")
            .long("port")
            .help("remote port to replay to")
            .default_value("50999")
        )
        .arg(Arg::with_name("client-port")
            .short("c")
            .long("client-port")
            .help("udp client port")
            .default_value("51999")
        )
        .arg(Arg::with_name("interval")
            .short("i")
            .long("print-interval")
            .help("prints the status periodically at specified interval (in secs)")
            .default_value("10")
        )
        .arg(Arg::with_name("udp")
            .short("u")
            .long("udp")
            .help("replay to a udp port instead of tcp")
        )
        .arg(Arg::with_name("preserve")
            .short("d")
            .long("preserve-delimiter")
            .help("whether to preserve the delimiter of the log event that is being replayed")
        )
        .arg(Arg::with_name("file")
            .short("f")
            .long("replay-file")
            .help("file path to replay the events from (required for non-server mode)")
            .takes_value(true)
        )
        .arg(Arg::with_name("server")
            .short("s")
            .long("server")
            .help("start a TCP/UDP server instance for consuming the events")
        )
        .arg(Arg::with_name("stats-only")
            .long("stats-only")
            .help("only prints the stats in the server mode, instead of printing out the \
                data as well")
        )
        .get_matches();

    let server_mode = matches.is_present("server");
    let host = matches.value_of("host").unwrap();
    let port = matches.value_of("port").unwrap();
    let is_udp = matches.is_present("udp");
    let print_interval = matches.value_of("interval").unwrap().parse::<u64>().unwrap();

    if server_mode {
        let stats_only_mode = matches.is_present("stats-only");
        // start a tcp/udp server instance for consuming the messages
        if is_udp {
            udp_server_exec(host, port, print_interval, stats_only_mode, log);
        } else {
            tcp_server_exec(host, port, print_interval, stats_only_mode, log);
        }
    } else {
        let replay_file = match matches.is_present("file") {
            true => matches.value_of("file").unwrap(),
            false => {
                let _ = println!("Requires --replay-file argument. Use --help for more usage.");
                std::process::exit(1);
            }
        };

        let client_port = matches.value_of("client-port").unwrap();
        let preserve_delimiter = matches.is_present("preserve");
        let eps = matches.value_of("eps").unwrap();
        let lines = lines_from_file(replay_file, preserve_delimiter);
        let file_stats = analyze_file(&lines);
        let sleep_time_ns: u64 = 1e9 as u64 / eps.parse::<u64>().unwrap();
        let total_events = matches.value_of("total")
            .unwrap_or(file_stats.num_lines.to_string().as_str()).parse::<u64>().unwrap();

        info!(log, "Replay file is {}", replay_file);
        info!(log, "{:?}", file_stats);

        if is_udp {
            debug!(log, "Sleep interval between events is {} milli secs", sleep_time_ns as f64 / 1e6);
            udp_thread_exec(
                host,
                client_port,
                port,
                total_events,
                &lines,
                sleep_time_ns,
                print_interval,
                log);
        } else {
            tcp_thread_exec(host, port, total_events, &lines, print_interval, log);
        }
    }
}

/// Reads log events from files that needs to be replayed and buffers in memory
fn lines_from_file(file_path: &str, preseve_delimeter: bool) -> Vec<String> {
    let file = File::open(file_path).expect("Unable to open file");
    let buf = BufReader::new(file);

    let lines: Vec<String> = buf.lines().map(|l| l.expect("Could not parse line")).collect();

    if preseve_delimeter {
        lines.into_iter().map(|mut line| {
            line.push_str(DEFAULT_DELIMITER);
            line.to_owned()
        }).collect::<Vec<String>>()
    } else {
        lines
    }
}

/// Analyzes the file contents such as file size, number of lines, maximum length of a line in the
/// file
fn analyze_file(lines: &Vec<String>) -> FileStats {
    FileStats {
        file_size: lines.iter().map(|str| str.len()).fold(0, |acc, len| acc + len),
        max_file_length: lines.iter().map(|str| str.len()).max().unwrap_or(0),
        num_lines: lines.len()
    }
}

/// Initializes a TCP client that writes the events to the specified remote TCP server
///
/// # Arguments
///
/// * `host` - A string that represents remote TCP server hostname
/// * `port` - A string that represents remote TCP server port number
/// * `total_events` - Total number of events that needs to be streamed to the remote TCP server
/// * `lines` - Vector of file contents that needs to be streamed, as returned by the
///             `[lines_from_file()]` method
/// * `sleep_time_ns` - duration in nano seconds to represent how long the thread should sleep
///                     before each message is streamed to remote TCP server
/// * `printer_interval` - duration in seconds to represent how long the printer thread should
///                        sleep
fn tcp_thread_exec(
    host: &str,
    port: &str,
    total_events: u64,
    lines: &Vec<String>,
    printer_interval: u64,
    log: Logger,
) {
    let printer_stats = Arc::new(Mutex::new(PrintStats::new(log.clone())));
    let mut stream: TcpStream = match TcpStream::connect(format!("{}:{}", host, port)) {
        Ok(stream) => stream,
        Err(e) => panic!("Error! Failed connecting to TCP socket {}:{} - {}", host, port, e)
    };

    let printer_stats_1 = printer_stats.clone();
    thread::spawn(move || {
        print_stats(printer_stats_1, printer_interval);
    });

    let now = Instant::now();
    let mut total_events_sent = 0;
    let mut total_bytes_sent = 0;
    for _ in 0..total_events {
        for line in lines.iter() {
            match stream.write(line.as_bytes()) {
                Ok(bytes) =>  {
                    total_events_sent += 1;
                    total_bytes_sent += bytes;
                    printer_stats.lock().unwrap().increment_event();
                    printer_stats.lock().unwrap().update_bytes(bytes as u64);
                },
                Err(e) => { panic!("Error! - {}", e); }
            }
        }
    }
    let elapsed = now.elapsed();

    stream.shutdown(Shutdown::Both).unwrap();
    info!(log, "Total events sent: {} [{} bytes]. Time elapsed: {} secs {} nanosecs.",
        total_events_sent, total_bytes_sent, elapsed.as_secs(), elapsed.subsec_nanos());
}

/// Initializes a UDP client that writes the events to the specified remote UDP server
///
/// # Arguments
///
/// * `host` - A string that represents remote UDP server hostname
/// * `client_port` - A string that represents UDP client port number
/// * `server_port` - A string that represents remote UDP server port number
/// * `total_events` - Total number of events that needs to be streamed to the remote TCP server
/// * `lines` - Vector of file contents that needs to be streamed, as returned by the
///             `[lines_from_file()]` method
/// * `sleep_time_ns` - duration in nano seconds to represent how long the thread should sleep
///                     before each message is streamed to remote UDP server
/// * `printer_interval` - duration in seconds to represent how long the printer thread should
///                        sleep
fn udp_thread_exec(
    host: &str,
    client_port: &str,
    server_port: &str,
    total_events: u64,
    lines: &Vec<String>,
    sleep_time_ns: u64,
    printer_interval: u64,
    log: Logger,
) {
    let printer_stats = Arc::new(Mutex::new(PrintStats::new(log.clone())));

    let socket: UdpSocket = match UdpSocket::bind(format!("0.0.0.0:{}", client_port)) {
        Ok(socket) => {
            info!(log, "Successfully binded to socket: {}:{}", host, client_port);
            socket
        },
        Err(e) =>
            panic!("Error! Failed to bind a UDP socket {}:{}. Reason: {:?}", host, client_port, e)
    };

    let printer_stats_1 = printer_stats.clone();
    thread::spawn(move || {
        print_stats(printer_stats_1, printer_interval);
    });

    let now = Instant::now();
    let mut loop_c: u64 = 0;
    while loop_c < total_events {
        for line in lines.iter() {
            thread::sleep(Duration::from_nanos(sleep_time_ns));

            match socket.send_to(line.as_bytes(), format!("{}:{}", host, server_port)) {
                Ok(bytes) => {
                    printer_stats.lock().unwrap().increment_event();
                    printer_stats.lock().unwrap().update_bytes(bytes as u64);
                },
                Err(e) =>
                    error!(
                        log,
                        "Failed to send data to UDP socket {}:{}. Reason: {:?}",
                        host,
                        server_port,
                        e
                    )
            };

            loop_c += 1;
            if loop_c == total_events {
                break;
            }
        }
    }
    let elapsed = now.elapsed(); 

    info!(log, "Total events attempted to send: {}. Time elapsed: {} secs {} nanosecs.",
        loop_c, elapsed.as_secs(), elapsed.subsec_nanos());
}

/// Handles TCP client connection
///
/// # Arguments
///
/// * `stream` - TcpStream to read events from
/// * `print_stats` - PrintStats struct
/// * `stats_only` - whether to print stats only or to print received data as well
fn handle_tcp_client(
    mut stream: TcpStream,
    print_stats: Arc<Mutex<PrintStats>>,
    stats_only: bool,
    log: Logger,
) {
    let mut buf = [0u8; BUFFER_SIZE];

    loop {
        match stream.read(&mut buf) {
            Ok(bytes) => {
                if bytes != 0 {
                    print_stats.lock().unwrap().increment_event();
                    print_stats.lock().unwrap().update_bytes(bytes as u64);
                    if !stats_only {
                        println!("{}", str::from_utf8(&buf).expect("Could not write buffer as string"));
                    }
                }
            },
            Err(e) => error!(log, "Could not read into buffer. Reason: {}", e)
        };

    }
}

/// Starts a TCP server instance that acts as an echo server which prints the events as received
///
/// # Arguments
///
/// * `host` - A string that represents which address to bind to
/// * `port` - A string that represents which port to bind to
/// * `printer_interval` - duration in secs
/// * `stats_only` - whether to print stats only or to print received data as well
fn tcp_server_exec(
    host: &str,
    port: &str,
    printer_interval: u64,
    stats_only: bool,
    log: Logger,
) {
    let printer_stats = Arc::new(Mutex::new(PrintStats::new(log.clone())));
    let stream: TcpListener = match TcpListener::bind(format!("{}:{}", host, port)) {
        Ok(socket) => {
            info!(log, "Successfully binded to socket: {}:{}", host, port);
            socket
        },
        Err(e) => panic!("Error! Failed to bind a TCP Socket {}:{}. Reason: {:?}", host, port, e)
    };

    let printer_stats_1 = printer_stats.clone();
    thread::spawn(move || {
        print_stats(printer_stats_1, printer_interval);
    });

    for stream in stream.incoming() {
        match stream {
            Ok(stream) => {
                let printer_stats_2 = printer_stats.clone();
                let log = log.clone();
                thread::spawn(move || {
                    handle_tcp_client(stream, printer_stats_2, stats_only, log);
                });
            },
            Err(e) => error!(log, "Failed to handle tcp client. Reason: {:?}", e)
        }
    }

}

/// Starts a UDP server instance that acts as an echo server which prints the events as received
///
/// # Arguments
///
/// * `host` - A string that represents which address to bind to
/// * `port` - A string that represents which port to bind to
/// * `printer_interval` - duration in secs
/// * `stats_only` - whether to print stats only or to print received data as well
fn udp_server_exec(
    host: &str,
    port: &str,
    printer_interval: u64,
    stats_only: bool,
    log: Logger,
) {
    // TODO need to set the buffer size based on the MTU of the interface, right now its safe
    // to assume MTU is 1500 bytes
    let mut buf = [0u8; 1500];
    let printer_stats = Arc::new(Mutex::new(PrintStats::new(log.clone())));

    let socket = match UdpSocket::bind(format!("{}:{}", host, port)) {
        Ok(socket) => {
            info!(log, "Successfully binded to socket: {}:{}", host, port);
            socket
        },
        Err(e) => panic!("Error! Failed to bind a UDP Socket: {}:{}. Reason: {:?}", host, port, e)
    };

    let printer_stats_1 = printer_stats.clone();
    thread::spawn(move || {
        print_stats(printer_stats_1, printer_interval);
    });

    loop {
        match socket.recv_from(&mut buf) {
            Ok((bytes, _)) => {
                printer_stats.lock().unwrap().increment_event();
                printer_stats.lock().unwrap().update_bytes(bytes as u64);
            },
            Err(e) => error!(log, "Could not read into buffer. Reason: {}", e)
        };

        if !stats_only {
            println!("{}", str::from_utf8(&buf).expect("Could not write buffer as string"));
        }
    }
}

///
/// Periodically prints the printer stats
///
/// # Arguments
///
/// * `ps` - PrintStats struct
/// * `sleep_interval` - duration in seconds
fn print_stats(ps: Arc<Mutex<PrintStats>>, sleep_interval: u64) {
    loop {
        thread::sleep(Duration::from_secs(sleep_interval));

        ps.lock().unwrap().print();
    }
}
